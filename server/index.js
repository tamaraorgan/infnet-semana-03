import('./src/server')

import { version, name } from './package.json'

console.log(`${name} v${version}`)
