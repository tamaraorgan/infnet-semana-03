module.exports = (sequelize, DataTypes) => {
  const Registration = sequelize.define('Registration', {
    name: DataTypes.TEXT,
    email: DataTypes.TEXT,
    birth_date: DataTypes.DATE
  })
  Registration.associate = models => {
    Registration.belongsTo(models.Course, {
      foreignKey: 'course_id',
      as: 'courses'
    })
  }
  return Registration
}
