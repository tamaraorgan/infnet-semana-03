module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    coordinator: DataTypes.TEXT,
    name: DataTypes.TEXT,
    status: DataTypes.BOOLEAN
  })
  Course.associate = models => {
    Course.hasMany(models.Registration, {
      foreignKey: 'course_id',
      as: 'registrations'
    })
  }
  return Course
}
