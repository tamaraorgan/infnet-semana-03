import express from 'express'
import cors from 'cors'
import router from './api/routes/router'


const app = express()
const port = process.env.PORT || 3333

app.use(cors())
app.use(express.json())
router(app)

app.listen(port, () => {
  console.log(`ᕦ(ツ)ᕤ Server starded on port ${port}!`)
})

module.exports = app


