const db = require('../../models/index')

module.exports = {
  async index(request, response) {
    const courses = await db.Course.findAll()
    
    return response.json(courses)
  },

  async create(request, response) {
    const { id, name, coordinator, status } = request.body

    const courses = await db.Course.create({
      id,
      name,
      coordinator,
      status
    })

    return response.json(courses)
  }
}
