import express from 'express'
import { name, version } from '../../../package.json'
import courseRoutesV1 from '../routes/v1/course.routes'
import registrationRoutesV1 from '../routes/v1/registration.routes'
import userRoutesV2 from './v2/user.routes'

module.exports = app => {
  const routerV1 = express.Router()
  const routerV2 = express.Router()

  app.get('/', (request, response) => {
    response.send({ name, version })
  })

  courseRoutesV1(routerV1)
  registrationRoutesV1(routerV1)
  
  userRoutesV2(routerV2)

  app.use('/v1', routerV1)
  app.use('/v2', routerV2)
}
